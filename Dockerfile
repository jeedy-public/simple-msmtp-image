FROM cern/cc7-base

LABEL maintainers.1="Ioannis Panagiotidis <ioannis.panagiotidis@cern.ch>"

RUN yum install -y dnf && \
    yum clean all && rm -rf /var/cache/yum

RUN dnf -y update && \
    dnf --setopt=tsflags=nodocs install -y \
    jq \
    msmtp \
    sendmail && \
    dnf clean all && \
    rm -rf /var/cache/dnf/*

ARG DUMB_INIT_VERSION
ARG DUMB_INIT_SHA256

RUN curl -s -L -o /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v${DUMB_INIT_VERSION}/dumb-init_${DUMB_INIT_VERSION}_amd64 && \
    echo "$DUMB_INIT_SHA256  /usr/local/bin/dumb-init" | sha256sum -c - && \
    chmod +x /usr/local/bin/dumb-init

RUN groupadd -g 1000 dbjeedy && useradd -u 1000 -m -g 1000 dbjeedy
USER dbjeedy

# TODO set workdir to the level of scripts directory
ENTRYPOINT ["/usr/local/bin/dumb-init","--"]
